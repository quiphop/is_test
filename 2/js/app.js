var app = angular.module('app', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('/list');
  $stateProvider
    .state('list', {
      url: '/list',
      template: "<div record-table records='records'></div>",
      controller: 'listCtrl'
    })
    .state('add', {
      url: '/list/add',
      template: '<div add-or-edit-record></div>',
    })
    .state('edit', {
      url: '/list/edit/:id',
      template: '<div add-or-edit-record></div>',
    })
});

app.directive("recordTable", function($window) {
  return{
    restrict: "EA",
    templateUrl: 'partials/table.html',
    scope: {
    	records: '='
    },
    controller: function($scope, recordService){
    	$scope.deleteRecord = function(id, index){
    		recordService.deleteRecord(id);
    	}
    }
  };
});

app.directive("addOrEditRecord", function($window) {
  return{
    restrict: "EA",
    templateUrl: 'partials/add.html',
    controller: function($scope, recordService, $stateParams, $state){
    	
    	$scope.record = {
    		id: $stateParams.id || Date.now(),
    		value: $stateParams.id ? localStorage.getItem($stateParams.id) : ''
    	};

    	$scope.saveRecord = function(){
    		recordService.createRecord($scope.record);
    		$state.go('list');
    	};
    }
  };
});

app.service('recordService', function() {
    
    this.getAll = function () {
      return localStorage;
    };

    this.createRecord = function(obj){
    	localStorage.setItem(obj.id, obj.value);
    };

    this.deleteRecord = function(id){
			localStorage.removeItem(id);
    };
});

app.controller('listCtrl', function($scope, $stateParams, recordService) {
	function init(){
		$scope.records = recordService.getAll();
	}

	init();
});
