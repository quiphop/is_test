var app = angular.module('app', []);


app.directive("tabsTable", function($window) {
  return {
    restrict: "AE",
    templateUrl: 'partials/table.html',
    scope: {
      points: '='
    },
    controller: function($scope) {
      this.addChild = function(nestedDirective) {};
    }
  };
});

app.directive('graph', function() {
  return {
    scope: {
      renderPoints: '=renderPoints',
      type: '@type'
    },
    replace: false,
    restrict: 'AE',
    require: '^tabsTable',
    template: "<svg width='250' height='250'></svg>",
    link: function(scope, elem, attrs, controllerInstance) {
      controllerInstance.addChild(scope);
      var lineData = [];
      for (var i = 0; i <= 10; i++) {
        lineData.push({
          x: i,
          y: i
        });
      }
      var vis = d3.select(elem[0].children[0]),
        WIDTH = 250,
        HEIGHT = 250,
        MARGINS = {
          top: 10,
          right: 20,
          bottom: 20,
          left: 50
        },
        xRange = d3.scale.ordinal().rangeRoundBands([MARGINS.left, WIDTH - MARGINS.right], 0.1).domain(lineData.map(function(d) {
          return d.x;
        })),

        yRange = d3.scale.linear().range([HEIGHT - MARGINS.top, MARGINS.bottom]).domain([0,
          d3.max(lineData, function(d) {
            return d.y;
          })
        ]),

        xAxis = d3.svg.axis()
        .scale(xRange)
        .tickSize(5)
        .tickSubdivide(true),

        yAxis = d3.svg.axis()
        .scale(yRange)
        .tickSize(5)
        .orient("left")
        .tickSubdivide(true);

      vis.append('svg:g')
        .attr('class', 'x axis')
        .attr('transform', 'translate(0,' + (HEIGHT - MARGINS.bottom) + ')')
        .call(xAxis);

      vis.append('svg:g')
        .attr('class', 'y axis')
        .attr('transform', 'translate(' + (MARGINS.left) + ',0)')
        .call(yAxis);
      switch (scope.type) {
        case 'points':
          vis.selectAll('circle')
            .data(scope.renderPoints)
            .enter()
            .append('circle')
            .attr('cx', function(d) {
              return xRange(d.x);
            })
            .attr('cy', function(d) {
              return yRange(d.y);
            })
            .attr("r", 3)
            .attr('fill', '#dd14cd');
          break;
        case 'chart':
          vis.selectAll('rect')
            .data(scope.renderPoints)
            .enter()
            .append('rect')
            .attr('x', function(d) {
              return xRange(d.x);
            })
            .attr('y', function(d) {
              return yRange(d.y);
            })
            .attr('width', xRange.rangeBand())
            .attr('height', function(d) {
              return ((HEIGHT - MARGINS.bottom) - yRange(d.y));
            })
            .attr('fill', '#dd14cd');
          break;
      }
    }
  };
});

app.controller('homeCtrl', function($scope) {
  $scope.points = {
    table1: [],
    table2: []
  };

  function init() {
    var uniqueXArr = [];
    //Fill points for 1st table, prepare 2nd
    for (var i = 1; i <= 5; i++) {
      uniqueXArr.push(i * 2);
      $scope.points.table1.push({
        x: Math.floor((Math.random() * 10) + 1),
        y: Math.floor((Math.random() * 10) + 1)
      });

    };
    shuffle(uniqueXArr);
    for (var i = 0; i < 5; i++) {
      $scope.points.table2.push({
        x: uniqueXArr[i],
        y: Math.floor((Math.random() * 10) + 1)
      });
    };
  }

  //Fisher-Yates algorithm
  function shuffle(array) {
    var currentIndex = array.length,
      temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }
  init();
});